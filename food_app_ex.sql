use app_food;
drop table if EXISTS orders;
drop table if EXISTS like_res;
drop table if EXISTS rate_res;
drop table if EXISTS users;
drop table if EXISTS restaurants;
drop TABLE if EXISTS sub_food;
drop TABLE if EXISTS foods;
drop table if EXISTS food_types;

-- Table USER

CREATE table users(
user_id int PRIMARY KEY auto_increment,
full_name VARCHAR(255) UNIQUE not NULL,
email VARCHAR(255),
password VARCHAR(100) not NULL
);

-- RESTAURANTS

CREATE table restaurants(
res_id int PRIMARY KEY auto_increment,
res_name VARCHAR(255) UNIQUE not NULL,
res_img VARCHAR(255),
res_desc VARCHAR(255)
);
-- RATE_RES

CREATE TABLE rate_res(
user_id int not NULL,
res_id int not NULL,
amount int not NULL,
date_rate DATETIME not NULL,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (res_id) REFERENCES restaurants (res_id),
PRIMARY KEY(user_id,res_id)
);

-- FOOD_TYPE

CREATE TABLE food_types(
type_id int PRIMARY KEY auto_increment,
type_name VARCHAR(255) not NULL
);


-- FOODS 

CREATE table foods(
food_id int PRIMARY KEY auto_increment,
food_name VARCHAR(255) not NULL,
food_img VARCHAR(255),
food_price FLOAT not NULL,
food_desc VARCHAR(255),
type_id int not NULL,
FOREIGN KEY (type_id) REFERENCES food_types(type_id)
);

-- ORDERS


CREATE table orders(
user_id int not NULL,
food_id int not NULL,
amount int not NULL,
code VARCHAR(255),
arr_sub_id VARCHAR(255),

FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (food_id) REFERENCES foods(food_id),
PRIMARY KEY (user_id,food_id)
);


-- LIKE_RES

CREATE TABLE like_res(
user_id int not NULL,
res_id int  not null,
date_like datetime not NULL,
FOREIGN KEY(user_id) REFERENCES users(user_id),
FOREIGN KEY(res_id) REFERENCES restaurants(res_id),
PRIMARY KEY (user_id,res_id)
);
-- SUB_FOOD

CREATE table sub_food(
sub_id int PRIMARY KEY auto_increment,
sub_name VARCHAR(255) not NULL,
sub_price FLOAT not NULL,
food_id int not NULL,
FOREIGN KEY(food_id) REFERENCES foods(food_id)
);


-- INSERT 


-- USER

INSERT INTO users(full_name,email,password) VALUES("Leigh Wall","enim.nisl@yahoo.com","VXN55LHT6FX"),("Rooney Holden","lectus.a.sollicitudin@outlook.org","FFE86BRB2BY"),
  ("Odette Mueller","volutpat.nulla.dignissim@yahoo.com","NBV80BUN6JL"),
  ("Kessie Morgan","aenean@yahoo.net","JKM26IRC2YM"),
  ("Kitra Clements","ut.quam.vel@hotmail.ca","DTC79TZD7FU"),("Tanisha Tate","curabitur@google.edu","ORN23HPY4BS"),
  ("Wing Kinney","vitae.risus.duis@icloud.ca","LKU28UJN7RQ"),
  ("Sydnee Wright","dui.cras@icloud.ca","DZP74WOY6KK"),
  ("Debra Barton","cursus.non.egestas@icloud.ca","ZII16OOL6ZV"),
  ("Allegra Drake","fusce.fermentum.fermentum@yahoo.ca","GLE46ORC9HF"),("Lois Shelton","quis@protonmail.org","GYC01QYO3XH"),
  ("Samuel Morrow","diam.proin@icloud.edu","KCC39NVE6CC"),
  ("Roary Glenn","orci@icloud.com","TQN81SUN7QN"),
  ("Alexandra Kirby","congue.elit@outlook.com","PCD77VKK8KC"),
  ("Chandler Bradford","donec.vitae@protonmail.net","BYD83GKO2NQ"), ("Griffin Wooten","sem.molestie@google.ca","FOE74JGP2DB"),
  ("Garrison Norris","commodo.hendrerit.donec@hotmail.couk","PSY33QYH3MR"),
  ("Cara Turner","quisque.libero@google.edu","MSC75YTY1OE"),
  ("Len Parker","consectetuer.ipsum.nunc@hotmail.ca","ZRC44NGI6PO"),
  ("Alden Leblanc","aliquet.molestie@aol.com","UKX52CNA3LO"),("Zia Cortez","at@protonmail.com","OIA98URH6SU"),
  ("Kevin Reid","ante.maecenas.mi@outlook.ca","ISJ84EGJ2TN"),
  ("Echo Daniels","ullamcorper@aol.couk","FTG31NIC1DG"),
  ("Vernon Knapp","aenean.egestas@google.edu","VKC05AAY5NO"),
  ("Kiara Everett","a.arcu@aol.net","IEV43GJB7PF");

-- RESRAUTANTS

INSERT INTO restaurants(res_name,res_img,res_desc)VALUES("Vel Convallis In Institute","http://nytimes.com?search=1","Sed malesuada augue ut lacus. Nulla tincidunt, neque"),
  ("Arcu Vel Limited","http://facebook.com?ab=441&aad=2","Sed nulla ante, iaculis nec, eleifend non,"),
  ("Quam Pellentesque Habitant Foundation","https://instagram.com?ab=441&aad=2","sociis natoque penatibus"),
  ("Fames Ac Corp.","http://walmart.com?ab=441&aad=2","at, nisi."),
  ("Pede Nunc Sed Foundation","http://zoom.us?ad=115","scelerisque dui. Suspendisse ac metus vitae velit"),
  ("Lobortis Institute","https://whatsapp.com?q=4","tempus eu,"),
  ("Semper Cursus PC","https://pinterest.com?ab=441&aad=2","neque sed sem egestas blandit. Nam nulla"),
  ("Mauris Incorporated","http://baidu.com?ad=115","lorem lorem, luctus ut,"),
  ("Et Magnis Inc.","https://bbc.co.uk?q=test","et, euismod et, commodo at, libero."),
  ("Sed PC","https://whatsapp.com?q=0","scelerisque scelerisque dui. Suspendisse ac metus vitae velit"),
   ("Elit Erat Corp.","http://instagram.com?str=se","Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim."),
  ("Non Enim Commodo Foundation","http://twitter.com?q=4","dui augue eu tellus. Phasellus"),
  ("Consectetuer Rhoncus Nullam Foundation","http://zoom.us?k=0","hendrerit neque. In ornare sagittis felis."),
  ("Fames Institute","https://nytimes.com?q=test","fames ac"),
  ("Nam Ligula LLC","http://bbc.co.uk?search=1","mattis. Cras eget nisi"),
   ("Dictum Associates","https://wikipedia.org?q=4","metus. Vivamus euismod urna. Nullam"),
  ("Cum Sociis Consulting","http://google.com?k=0","Cras vehicula aliquet libero."),
  ("Cras Associates","http://baidu.com?str=se","Donec egestas."),
  ("Pharetra Foundation","https://naver.com?p=8","nunc risus varius orci, in consequat enim"),
  ("Ridiculus Inc.","https://zoom.us?q=4","vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse"),
  ("Inceptos Company","https://twitter.com?q=4","nec, eleifend non, dapibus rutrum, justo. Praesent"),
  ("Enim Institute","https://walmart.com?g=1","amet lorem semper auctor. Mauris vel turpis. Aliquam"),
  ("Sem Semper LLP","https://whatsapp.com?gi=100","sem molestie sodales. Mauris blandit enim consequat"),
  ("Rutrum Magna Cras Company","https://twitter.com?p=8","Curabitur massa. Vestibulum accumsan neque et nunc. Quisque"),
  ("Sem Consequat Corp.","https://instagram.com?q=test","Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec");



-- rate res

INSERT INTO rate_res(user_id,res_id,amount,date_rate)VALUES (3,24,2,"2023-11-14 20:45:09"),
  (20,15,7,"2023-05-17 19:21:11"),
  (25,11,10,"2023-03-10 01:52:46"),
  (9,20,2,"2022-02-10 17:45:46"),
  (20,8,4,"2023-07-10 20:54:21"),
  (2,15,4,"2022-11-04 08:33:26"),
  (5,13,9,"2023-10-17 05:27:50"),
  (17,23,1,"2023-07-29 19:06:59"),
  (15,12,4,"2022-06-27 05:56:23"),
  (9,6,9,"2023-07-30 04:34:39"),
  (21,1,9,"2022-11-12 01:26:07"),
  (13,15,9,"2022-09-19 03:43:56"),
  (10,3,7,"2022-05-10 18:37:20"),
  (3,14,4,"2022-02-25 08:27:50"),
  (20,22,3,"2023-06-17 03:57:34"),
  (18,4,5,"2023-03-03 14:55:39"),
  (1,14,4,"2022-07-29 03:32:56"),
  (12,3,9,"2023-09-02 19:55:53"),
  (5,8,4,"2022-07-07 19:16:14"),
  (3,21,2,"2022-08-27 22:47:12"),
  (18,24,6,"2022-12-23 01:09:11"),
  (9,17,3,"2023-06-09 16:12:52"),
  (23,2,1,"2022-10-11 17:06:02"),
  (6,8,7,"2022-02-21 14:15:15"),
  (10,23,8,"2023-07-22 10:56:23"),
  (15,3,7,"2023-06-01 12:51:30"),
  (11,4,8,"2022-09-03 21:07:23"),
  (7,3,9,"2023-09-22 20:55:02"),
  (12,9,6,"2023-08-06 19:59:53"),
  (5,11,8,"2023-01-08 22:18:40"),
  (21,4,5,"2023-10-11 08:29:40"),
  (22,13,7,"2022-10-12 07:39:24"),
  (13,2,8,"2023-09-10 21:47:57"),
  (20,21,3,"2023-08-31 05:57:00"),
  (4,11,7,"2023-07-21 22:22:55");
  
  
  -- food type
  
INSERT INTO food_types(type_name)VALUES ("diam"),
  ("iaculis"),
  ("tellus."),
  ("elementum"),
  ("mollis"),
  ("vehicula"),
  ("Maecenas"),
  ("Integer"),
  ("dictum"),
  ("magna.");



-- food 

INSERT INTO foods(food_name,food_img,food_price,food_desc,type_id)VALUES
 ("Bun bo","https://baidu.com/en-us?q=4",77,"aptent taciti sociosqu ad",7),
  ("hu tieu","https://yahoo.com/settings?gi=100",63,"fringilla cursus purus.",4),
  ("pho","http://wikipedia.org/en-us?gi=100",62,"auctor, velit eget laoreet posuere, enim nisl elementum purus,",4),
  ("com tam","https://walmart.com/user/110?q=test",68,"feugiat metus",4),
  ("chao long","http://twitter.com/sub/cars?page=1&offset=1",72,"lectus justo eu arcu. Morbi",6),
   ("banh canh","https://youtube.com/fr?gi=100",93,"mauris ut",3),
  ("nui","http://ebay.com/fr?ad=115",21,"id sapien. Cras dolor dolor, tempus",6),
  ("mi xao bo","http://twitter.com/site?gi=100",85,"enim consequat",2),
  ("ga chien","https://youtube.com/sub/cars?q=0",43,"Lorem ipsum dolor sit",6),
  ("ga xao","http://cnn.com/en-ca?g=1",49,"a, auctor",9),
  ("bo kho","https://wikipedia.org/sub?p=8",44,"Cras vulputate velit eu",6),
  ("bot chien","http://naver.com/site?g=1",47,"Aliquam erat volutpat. Nulla facilisis.",4),
  ("mi do","http://baidu.com/group/9?g=1",67,"orci lacus vestibulum",5),
  ("com chien","https://twitter.com/en-ca?page=1&offset=1",71,"neque. Sed eget lacus. Mauris non dui",5),
  ("tau hu","https://google.com/user/110?search=1",95,"a neque. Nullam",6);
  
  
  -- oder

  
  INSERT INTO orders(user_id,food_id,amount,code,arr_sub_id)VALUES (19,12,8,"WFC23FKG3LX","Cras interdum. Nunc sollicitudin"),
  (6,6,2,"GFX97AYN8UG","Integer"),
  (24,13,9,"AVN85CJO7QD","hendrerit"),
  (19,14,9,"LET79JLK4OW","Phasellus ornare. Fusce"),
  (14,3,9,"EEJ49JPU0KV","risus. Quisque"),
    (14,14,9,"KQH25PZR1FL","dignissim magna"),
  (9,9,8,"YVW68XTG8RP","neque tellus, imperdiet"),
  (2,10,8,"JIW85IPI8AH","Nam consequat"),
  (3,5,4,"BOT39BDK1ZP","neque"),
  (5,5,1,"YLD17YKB3MC","magna. Suspendisse tristique"),
   (15,13,3,"PDG32BAH7OQ","Nunc quis"),
  (20,2,10,"ATN09TAQ1PE","at pede."),
  (18,14,5,"CBA93JTD3OR","aliquet libero. Integer"),
  (17,8,8,"EXU21FPD3TN","fringilla. Donec"),
  (21,6,4,"DRG25LFT2VD","enim. Etiam"),
  (13,1,6,"OHW44RJK0CJ","enim. Etiam"),
  (3,6,8,"SDC02XHP4KE","Aliquam ultrices iaculis"),
  (22,2,10,"FLV24ILS1LB","id, libero."),
  (18,13,9,"MIN46NIW2OM","primis in"),
  (10,9,7,"YJI37IUG7HP","quam");
  
  
  



-- Like res

INSERT INTO like_res(user_id,res_id,date_like)VALUES
  (5,3,"2023-01-31 14:49:27"),
  (14,11,"2023-01-29 13:27:56"),
  (13,12,"2022-01-04 14:03:11"),
  (20,5,"2023-09-20 11:00:16"),
  (2,18,"2022-02-03 17:49:57"),
   (17,9,"2022-06-07 11:03:31"),
  (8,8,"2022-05-11 01:01:12"),
  (2,1,"2023-07-06 01:03:53"),
  (24,9,"2022-07-31 16:04:21"),
  (19,9,"2023-03-07 15:44:12"),
   (15,3,"2023-10-28 00:35:37"),
  (6,7,"2022-09-28 21:39:46"),
  (7,5,"2022-06-23 22:35:08"),
  (17,14,"2023-02-14 20:49:10"),
  (11,21,"2023-01-29 23:54:04"),
    (21,20,"2023-03-13 17:15:03"),
  (22,8,"2022-04-02 07:43:48"),
  (17,16,"2022-01-04 19:05:00"),
  (25,20,"2023-03-06 04:17:48"),
  (4,13,"2023-03-04 23:54:34");


-- sub food


INSERT INTO sub_food(sub_name,sub_price,food_id)VALUES("Donec",78,13),
  ("turpis",20,5),
  ("molestie",33,7),
  ("eu",64,4),
  ("ipsum",44,5),
   ("tortor.",14,1),
  ("quis,",18,4),
  ("Nulla",14,11),
  ("mi",39,13),
  ("dui.",19,3),
   ("molestie",97,9),
  ("ipsum",13,6),
  ("nostra,",53,9),
  ("molestie",40,5),
  ("venenatis",87,8);






-- BÀI LÀM



 -- tìm 5 người đã like nhà hàng nhiều nhất 
 SELECT users.full_name, COUNT(*) as total_like FROM users
 INNER JOIN like_res
 ON users.user_id = like_res.user_id
 GROUP BY full_name
 ORDER BY total_like DESC
 LIMIT 5;
 
 
 -- tìm 2 nhà hàng có lượt like nhiều nhất 
 
 SELECT restaurants.res_name, COUNT(*) AS total_like FROM restaurants 
 INNER JOIN like_res
 ON like_res.res_id = restaurants.res_id
 GROUP BY res_name
 ORDER BY total_like DESC
 LIMIT 2
 ;




-- Tìm người đã đặt hàng nhiều nhất

SELECT full_name , COUNT(*) as total_order FROM users
INNER JOIN orders
on orders.user_id = users.user_id
GROUP BY full_name
ORDER BY total_order DESC
LIMIT 1
;


-- Tìm người dùng không hoạt động

SELECT full_name FROM users 
LEFT JOIN like_res
ON like_res.user_id = users.user_id
LEFT JOIN rate_res 
on rate_res.user_id = users.user_id
LEFT JOIN orders
on orders.user_id = users.user_id
WHERE orders.user_id is NULL && like_res.user_id is NULL && rate_res.user_id is NULL;


-- Tính trung bình food sub của 1 sub

SELECT COUNT(*) FROM sub_food;
SELECT COUNT(*) FROM foods;

SELECT ((SELECT COUNT(*) FROM sub_food) / (SELECT COUNT(*) FROM foods)) as trung_binh;
